from .. import fidx

def test_bytearray():
  assert fidx.bytearray([1,56,0,155])[.5] == bytearray([1,56,0,155])[2]
