from .. import fidx

def test_custom_class():
  class arr(list):
    @fidx.param(1, 2, 'i', 'j')
    def sum(self, i = 0, j = 0): return self[i]+self[j]

  a = arr([1,2,3])

  assert arr.sum(a,.5, 2.) == 5
  assert a.sum(j=.4) == 3
  assert a.sum(i=.4) == 3
  assert a.sum(j=.1, i=.9) == 4
