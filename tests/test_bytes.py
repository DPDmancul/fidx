from .. import fidx

def test_bytes():
  assert fidx(b'test')[.5] == b'test'[2]
