from .. import fidx

def test_str():
  s = fidx.str('Mandi')
  t = fidx('Hello')
  u = fidx(fidx('Ciao'))
  assert s[1/2] == 'n'
  assert t[.5] == 'l'
  assert u[1/4] == 'i'
  assert t[:-1.] == 'Hell'
  assert s[::2/5] == 'Mni'

def test_epsilon():
  ε = fidx('')
  e = fidx.str()
  assert ε == e

def test_name():
  assert fidx.str.__qualname__ == fidx.str.__name__ == 'str'
