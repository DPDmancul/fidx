from .. import fidx
import numpy as np

fidx.add_numpy()

def eq_all(*a : np.ndarray):
  first = next(it := iter(a))
  return np.all(first == e for e in it)


def test_numpy():
  a = np.array([1,2,3])
  b = fidx(a)
  c = fidx.nparray(a)
  d = fidx(c)
  assert a[1] == b[.5] == c[1.] == d[(1.,)]
  assert d.max() == a.max()
  assert list(d[d<3]) == [1,2]
  assert eq_all(d[(0.,.5),], d[[0.,.5]], d[fidx((0.,.5)),], d[fidx([0.,.5])], d[:-1])
  assert list(d[...]) == list(d[(...,)]) == [1,2,3]
  assert (e := d[np.newaxis, None])[0,0.,.5] == a[1]
  assert (e[0,...] == e[0]).all()
  assert eq_all(e[0,0.,:2], e[fidx((0,0,(0.,.5)))], e[0,0,fidx((0.,.5))], e[0,0,fidx([0.,.5])], e[0,0,[0.,.5]], e[0,0,fidx.nparray([0.,.5])], e[0,0,np.array([0.,.5])])
  assert eq_all(e[0,0,fidx([[0.,.5],[1,.9]])], e[0,0,[[0.,.5],[1,.9]]], e[0,0,fidx.nparray([[0.,.5],[1,.9]])], e[0,0,np.array([[0.,.5],[1,.9]])] == np.array([[1,2],[2,3]]))
  np.random.shuffle(f := d.copy())
  assert set(d) == set(f)
  g = e[0,0]
  assert (e[0,0,g>1] == [2,3]).all()

def test_edit():
  a = fidx.nparray([1,2,3])
  assert len(a) == 3
  a[.5] = 7
  assert list(a) == [1,7,3]
  a[[0,2]] = 7
  assert list(a) == [7,7,7]

def test_name():
  assert fidx.nparray.__qualname__ == fidx.nparray.__name__ == 'nparray'
