from .. import fidx

def test_one_based():
  a = list([1,2,3])
  fidx.add(list, name='one_based_list')
  fidx.one_based_list.set_index_map(int, lambda _, i: i-1, override=True)
  b = fidx(a)
  c = fidx.one_based_list(a)
  assert a[0] == b[0] == c[1]
  assert b[:1] == c[:2]

def test_bool_based():
  a = list([1,2,3])
  fidx.add(list, name='bool_indexed_list')
  fidx.bool_indexed_list.set_index_map(bool, lambda s, i: slice(None) if i else slice(0,0))
  b = fidx.bool_indexed_list(a)
  assert b[True] == a
  assert b[False] == []

def test_string():
  a = list([1,2,3])
  fidx.set_index_map(str, lambda _,i: int(i), global_map=True)
  b = fidx(a)
  assert a[0] == b['0']
  assert a[2] == b['2']
