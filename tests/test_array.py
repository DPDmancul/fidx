from .. import fidx
import array

fidx.add(array.array, name='arr')

def test_list():
  a = array.array('i', [1,2,3])
  b = fidx.arr('i', [1,2,3])
  assert a[1] == b[.5]

def test_init():
  a = fidx.arr('f', range(1,11))
  b = fidx.arr('f', [*range(1,11)])
  assert a == b

def test_edit():
  a = fidx.arr('i')
  assert len(a) == 0
  a.append(1)
  a.append(2)
  a.append(3)
  assert list(a) == [1,2,3]
  a[.5] = 7
  assert list(a) == [1,7,3]
  assert a.index(7) == 1
  del a[.5]
  assert list(a) == [1,3]
  a.insert(1/2, 2)
  assert list(a) == [1,2,3]

def test_split():
  a = fidx.arr('i', range(1,10))
  assert [*a[:.5], *a[.5:]] == list(a)
  assert a[:.5] + a[.5:] == a

def test_slice():
  a = fidx.arr('i', range(1,11))
  assert list(a[.4::-.2]) == [5, 3, 1]


def test_name():
  assert fidx.arr.__qualname__ == fidx.arr.__name__ == 'array'
