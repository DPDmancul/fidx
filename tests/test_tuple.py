from .. import fidx

def test_tuple():
  a = (1,2,3)
  b = fidx(a)
  c = fidx.tuple(a)
  d = fidx(c)
  print(a,b,c,d)
  assert a[1] == b[.5] == c[1.] == d[-2.]

def test_init():
  a = fidx.tuple(range(1,11))
  b = fidx((*range(1,11),))
  assert a == b

def test_split():
  a = fidx.tuple(range(1,10))
  assert (*a[:.5], *a[.5:]) == a

def test_slice():
  a = fidx.tuple(range(1,11))
  assert a[.4::-.2] == (5, 3, 1)

def test_name():
  assert fidx.tuple.__qualname__ == fidx.tuple.__name__ == 'tuple'
