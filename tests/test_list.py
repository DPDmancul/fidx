from .. import fidx

def test_list():
  a = [1,2,3]
  b = fidx(a)
  c = fidx.list(a)
  d = fidx(c)
  print(a,b,c,d)
  assert a[1] == b[.5] == c[1.] == d[-2.]

def test_init():
  a = fidx.list(range(1,11))
  b = fidx([*range(1,11)])
  assert a == b

def test_edit():
  a = fidx.list()
  b = fidx(a)
  assert len(a) == 0
  a.append(1)
  a.append(2)
  a.append(3)
  assert a == [1,2,3]
  a[.5] = 7
  assert a == [1,7,3]
  assert a.index(7, 0., .8) == 1
  del a[.5]
  assert a == [1,3]
  a.insert(1/2, 2)
  assert a == [1,2,3]
  assert a != b

def test_split():
  a = fidx.list(range(1,10))
  assert [*a[:.5], *a[.5:]] == a
  assert a[:.5] + a[.5:] == a

def test_slice():
  a = fidx.list(range(1,11))
  assert a[.4::-.2] == [5, 3, 1]


def test_name():
  assert fidx.list.__qualname__ == fidx.list.__name__ == 'list'
